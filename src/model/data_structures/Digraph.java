package model.data_structures;


import java.util.NoSuchElementException;

//Nota: esta clase fue sacada de la pagina del libro Algorithms 4th edition de Sedgewick y Wayne
/**
 *  The {@code Digraph} class represents a directed graph of vertices
 *  named 0 through <em>V</em> - 1.
 *  It supports the following two primary operations: add an edge to the digraph,
 *  iterate over all of the vertices adjacent from a given vertex.
 *  Parallel edges and self-loops are permitted.
 *  <p>
 *  This implementation uses an adjacency-lists representation, which 
 *  is a vertex-indexed array of {@link Bag} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the vertices adjacent from a given vertex, which takes
 *  time proportional to the number of such vertices.
 *  <p>
 *  For additional documentation,
 *  see <a href="https://algs4.cs.princeton.edu/42digraph">Section 4.2</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class Digraph {
    private static final String NEWLINE = System.getProperty("line.separator");
    private final int V;           // number of vertices in this digraph
    private int E;                 // number of edges in this digraph
    private Queue<Integer>[] adj;    // adj[v] = adjacency list for vertex v
    private int[] indegree;        // indegree[v] = indegree of vertex v
    private LinearProbingHash<String,String> pesosArcos;
    private ArregloFlexible<String> listaArcos;
    /**
     * Initializes an empty digraph with <em>V</em> vertices.
     *
     * @param  V the number of vertices
     * @throws IllegalArgumentException if {@code V < 0}
     */
    public LinearProbingHash<String, String> darPesosArcos(){
    	return pesosArcos;
    }
    public ArregloFlexible<String> darListaArcos(){
    	return listaArcos;
    }
    public Digraph(int V) {
    	pesosArcos = new LinearProbingHash<String,String>(300);
    	listaArcos = new ArregloFlexible<String>(400);
        if (V < 0) throw new IllegalArgumentException("Number of vertices in a Digraph must be nonnegative");
        this.V = V;
        this.E = 0;
        indegree = new int[V];
        adj = (Queue<Integer>[]) new Queue[V];
        for (int v = 0; v < V; v++) {
            adj[v] = new Queue<Integer>();
        }
    }

    

   
        
    /**
     * Returns the number of vertices in this digraph.
     *
     * @return the number of vertices in this digraph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in this digraph.
     *
     * @return the number of edges in this digraph
     */
    public int E() {
        return E;
    }


    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
    
    /**
     * Adds the directed edge v to w to this digraph.
     *
     * @param  v the tail vertex
     * @param  w the head vertex
     * @throws IllegalArgumentException unless both {@code 0 <= v < V} and {@code 0 <= w < V}
     */
    public void addEdge(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        adj[v].enqueue(w);
        indegree[w]++;
        E++;
    }
    public void addEdge(int v, int w, String peso) {
        validateVertex(v);
        validateVertex(w);
        String llave = v+"-"+w;
        try {
			pesosArcos.put(llave, peso);
			listaArcos.agregarElem(llave);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("No se pudo agregar el peso del arco");
		}
        adj[v].enqueue(w);
        indegree[w]++;
        E++;
    }

    /**
     * Returns the vertices adjacent from vertex {@code v} in this digraph.
     *
     * @param  v the vertex
     * @return the vertices adjacent from vertex {@code v} in this digraph, as an iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<Integer> adj(int v) {
        validateVertex(v);
        return adj[v];
    }

    /**
     * Returns the number of directed edges incident from vertex {@code v}.
     * This is known as the <em>outdegree</em> of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the outdegree of vertex {@code v}               
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int outdegree(int v) {
        validateVertex(v);
        return adj[v].size();
    }

    /**
     * Returns the number of directed edges incident to vertex {@code v}.
     * This is known as the <em>indegree</em> of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the indegree of vertex {@code v}               
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int indegree(int v) {
        validateVertex(v);
        return indegree[v];
    }
    /**
     * Returns the reverse of the digraph.
     *
     * @return the reverse of the digraph
     */
    public Digraph reverse() {
        Digraph reverse = new Digraph(V);
        for (int v = 0; v < V; v++) {
            for (int w : adj(v)) {
                reverse.addEdge(w, v);
            }
        }
        return reverse;
    }
   @Override
public String toString() {
	// TODO Auto-generated method stub
	   String retorno ="";
	   for(int i=0;i<listaArcos.darTamAct();i++) {
		   String llave = (String) listaArcos.darElementoI(i);
		   System.out.println(llave+": "+pesosArcos.get(llave));
	   }
	return retorno;
}

   //Aplica dfs
  public void DFSUtil(int v,boolean visitados[]) {
	  visitados[v] = true;
	  System.out.println("Se visit� el vertice: "+v);
	  int n;
	  Queue<Integer> queue=adj[v];
	  while(!queue.isEmpty()) {
		  n = queue.dequeue();
		  if(!visitados[n]) {
			  DFSUtil(n,visitados);
		  }
	  }
  }
  //retorna el reverso del grafo
  public Digraph darReverso() {
	  Digraph digraph = new Digraph(V);
	  for(int v=0;v<V;v++) {
		  Queue<Integer> queue = adj[v];
		  while(!queue.isEmpty()) {
			  digraph.adj[queue.dequeue()].enqueue(v);
		  }
	  }
	  return digraph;
  }
  
  public void fillOrder(int v, boolean visitados[],Stack<Integer> pila) {
	  visitados[v]=true;
	  
	  Queue<Integer> queue = adj[v];
	  while(!queue.isEmpty()) {
		  int n = queue.dequeue();
		  if(!visitados[n]) {
			  fillOrder(n, visitados, pila);
		  }
	  }
	  pila.push(v);
  }
  public void darComponentesFuertementeConectadas() {
	  Stack<Integer> pila = new Stack<Integer>();
	  boolean[] visitados = new boolean[V];
	  boolean[] visitados2 = new boolean[V];
	  for(int i=0;i<V;i++) {
		  visitados[i]=false;
		  visitados[i]=false;
	  }
	  
	  for(int i=0;i<V;i++) {
		  if(!visitados[i]) {
			  fillOrder(i,visitados,pila);
		  }
	  }
	  
	  Digraph reverso = darReverso();
	  int contador=0;
	  while(!pila.isEmpty()) {
		  int v = pila.pop();
		  if(!visitados2[v]) {
			  reverso.DFSUtil(v, visitados2);
			  contador++;
		  }
	  }
	  System.out.println(contador+"");
  }

 

}