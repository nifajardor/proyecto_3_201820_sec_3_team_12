package model.data_structures;

import java.util.Iterator;

import model.data_structures.Queue.ListIterator;
import model.data_structures.Queue.Nodo;



	
public class Stack<T> implements Iterable<T> {
	private Nodo<T> primero;
	private Nodo<T> ultimo;
	private int tamanio;
	public static class Nodo<T>{
		private T elemento;
		private Nodo<T> siguiente;
		public Nodo(T elem,Nodo<T> sig){
			elemento=elem;
			siguiente=sig;
		}
	}
	public Stack() {
		primero = null;
		ultimo = null;
		tamanio = 0;
	}
	public boolean isEmpty() {
		boolean r = false;
		if(primero == null) {
			r = true;
		}
		return r;
	}
	public int size() {
		return tamanio;
	}
	public T peek() {
		T r = null;
		if(primero != null) {
			r = primero.elemento;
		}
		return r;
	}
	public void push(T pElem) {
		if(primero==null){
			primero=new Nodo(pElem,null);
			ultimo=new Nodo(pElem,null);
		}else{
			Nodo<T> temp=new Nodo(pElem,primero);
			ultimo=primero;
			primero=temp;
			
		}
		tamanio++;
	}
	public T pop() {
		Nodo<T> temp=primero;
	primero=primero.siguiente;
	tamanio--;
	return temp.elemento;
	}
	
	public Iterator<T> iterator(){
		return new ListIterator<T>(primero);
	}
	
	public class ListIterator<T> implements Iterator<T>{
		private Nodo<T> actual;
		public ListIterator(Nodo<T> pPrimero){
			actual = pPrimero;
		}
		public boolean hasNext() {
			boolean r = false;
			if(actual != null) {
				r = true;
			}
			return r;
		}
		public T next() {
			T r = null;
			if(hasNext()) {
				r = actual.elemento;
				actual = actual.siguiente;
			}
			return r;
		}
		
	}
	
}

