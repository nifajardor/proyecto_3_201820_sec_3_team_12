package model.data_structures;


import java.io.Serializable;

import model.vo.*;


public class Grafo<K extends Comparable<K>,V,A> implements Serializable,IGraph {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String NEWLINE = System.getProperty("line.separator");

    private int E;

    private LinearProbingHash<K, V> vertices;
    private LinearProbingHash<String, A> arcos;
    public Grafo() {
        vertices = new LinearProbingHash<>();
        arcos = new LinearProbingHash<>();
    }
   public int V(){
    	return vertices.size();
    }
   public int E(){
    	return arcos.size();
    }
   public void addEdge(K key1,K key2, A arco){
	   E++;
	   try {
		  ((Vertice)vertices.get(key1)).anadirArco((Arco)arco);
		  ((Vertice)vertices.get(key2)).anadirArco((Arco)arco);
		 
		arcos.put(key1 +"-"+ key2, arco);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }
   public void addVertex(K key,V valor){
	   try {
			vertices.put(key,valor);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   }
   public V getInfoVertex(K key) throws Exception{
	   
	  if( vertices.get(key)==null){
		  throw new Exception("El vertice " + key + " no existe.");
	  }else{
		  return vertices.get(key);
	  }
   }
   public A getInfoArc(K key1,K key2) throws Exception{
		  if( arcos.get(key1+"-"+key2)==null){
			  throw new Exception("El arco " +key1+"-"+key2 + " no existe.");
		  }else{
			  return arcos.get(key1+"-"+key2);
		  }
	   }
   public void setInfoVertex(K key,V valor)throws Exception{
	   if(vertices.get(key) != null){
		   	vertices.delete(key);
		   	addVertex(key, valor);
	   }else{
		   throw new Exception("El vertice " + key + " no existe.");
	   }
		   
   }
   public void setInfoArc(K key1,K key2, A arc)throws Exception{
	   if(arcos.get(key1+"-"+key2) != null){
		   	arcos.delete(key1+"-"+key2);
		   	addEdge(key1,key2, arc);
	   }else{
		   throw new Exception("El arco " +key1+"-"+key2 + " no existe.");
	   }
		   
   }
   public Iterable<K> adj(K idVertex) {
       String[]st = new String[3];
       Iterable<String> KArcos = arcos.keys();
       Stack<K> adyacencias = new Stack<K>();
       for(String a: KArcos)
       {
           st[0] = a.split("-")[0];
           st[1] = a.split("-")[1];
           if(idVertex.equals(st[0]))
           {
               adyacencias.push((K) st[1]);
           }
           if(idVertex.equals(st[1]))
           {
               adyacencias.push((K) st[0]);
           }
       }
       System.out.println(adyacencias.size());
       return adyacencias;
   }


}