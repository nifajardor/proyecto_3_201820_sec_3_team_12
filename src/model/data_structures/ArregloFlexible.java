package model.data_structures;

public class ArregloFlexible<T> {
	public static final int START = 0;
	public static final int STOP = 1;
	private int tamanoMax;
	private int tamanoAct;
	public T elementos[];

	public ArregloFlexible(int max){
		elementos=(T[])new Object[max];
		tamanoMax=max;
		tamanoAct=0;
	}
	public void agregarElem(T dato){
		if(tamanoAct==tamanoMax){
			tamanoMax=2*tamanoMax;
			T[] copia=elementos;
			elementos=(T[])new Object[tamanoMax];
			for(int i=0;i<tamanoAct;i++){
				elementos[i]=copia[i];
			}
		}

		elementos[tamanoAct] = dato;

		tamanoAct++;
	}

	public T darElementoI(int i) {
		return elementos[i];
	}
	public int darTamAct() {
		return tamanoAct;
	}

	public void anadirElementoPos(int pos, T elem) {
		elementos[pos] = elem;
	}

	public int darTamano(){
		return tamanoAct;
	}
	public T[] darElementos(){

		return elementos;
	}
	
	

	
}
