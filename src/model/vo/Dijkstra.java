package model.vo;

import model.data_structures.ArregloFlexible;
import model.data_structures.Cola;
import model.data_structures.ColaPrioridad;
import model.data_structures.Grafo;
import model.data_structures.LinearProbingHash;

public class Dijkstra {

	private final int MAX = 10005;  //maximo numero de v�rtices
    private final int INF = 1<<30;  //definimos un valor grande que represente la distancia infinita inicial, basta conque sea superior al maximo valor del peso en alguna de las aristas
    
    private ArregloFlexible ady = new ArregloFlexible(10); //lista de adyacencia
    private LinearProbingHash<String,Double> distancias=new LinearProbingHash<String,Double>();  
    private LinearProbingHash<String,Boolean> visitados=new LinearProbingHash<String,Boolean>();  //distancia[ u ] distancia de v�rtice inicial a v�rtice con ID = u
    private boolean visitado[ ] = new boolean[ MAX ];   //para v�rtices visitados
    private ColaPrioridad<Vertice<String>> Q = new ColaPrioridad<Vertice<String>>(10); //priority queue propia de Java, usamos el comparador definido para que el de menor valor este en el tope
   private Cola<Vertice<String>> cola=new Cola();
    private int V;       
    double suma=0;//numero de vertices
    private LinearProbingHash<String,Vertice<String>> previo  =new  LinearProbingHash<String,Vertice<String>>();  //para la impresion de caminos
    private boolean dijkstraEjecutado;
    private Grafo<String,Vertice<String>,Arco> grafo;
   public Dijkstra(Grafo<String,Vertice<String>,Arco> grafo){
        this.grafo=grafo;
    }
    
    //En el caso de java usamos una clase que representara el pair de C++

    
    //funci�n de inicializaci�n


    //Paso de relajacion
    public void relajacion( Vertice<String> actual , Vertice<String> adyacente , double peso ){
        //Si la distancia del origen al vertice actual + peso de su arista es menor a la distancia del origen al vertice adyacente
    	try{
    		if(distancias.get(adyacente.darId())==null){
    			distancias.put(adyacente.darId(),Double.MAX_VALUE);
    		}
        if( distancias.get(actual.darId()) + peso < distancias.get(adyacente.darId()) ){
            distancias.put(adyacente.darId(),distancias.get(actual.darId())+peso);  //relajamos el vertice actualizando la distancia
            suma=suma+peso;
            previo.put(adyacente.darId(), actual);                       //a su vez actualizamos el vertice previo
            cola.enqueue(adyacente); //agregamos adyacente a la cola de prioridad
           
        }
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    }

   public void dijkstra( Vertice<String> inicial ){
        try{
        cola.enqueue(inicial); //Insertamos el v�rtice inicial en la Cola de Prioridad
        distancias.put(inicial.darId(), 0.0);   //Este paso es importante, inicializamos la distancia del inicial como 0
        Vertice<String>  adyacente ;
        double peso=0;
        Vertice<String>actual;
        while( !cola.isEmpty() ){                   //Mientras cola no este vacia
            actual = cola.dequeue();            //Obtengo de la cola el nodo con menor peso, en un comienzo ser� el inicial
                         //Sacamos el elemento de la cola
            
            if( !actual.darVisitado() ){  //Si el v�rtice actual ya fue visitado entonces sigo sacando elementos de la cola
            actual.visitar();    //Marco como visitado el v�rtice actual
            
           
            	
            
            for( int i = 0 ; i < actual.darInfovertice().darAdjacencias().darTamAct() ; i++ ){ //reviso sus adyacentes del vertice actual
                adyacente = actual.darInfovertice().darAdjacencias().darElementoI(i);   //id del vertice adyacente
                //System.out.println(adyacente.darId());
                peso = grafo.getInfoArc(actual.darId(), adyacente.darId()).darCosto();      //peso de la arista que une actual con adyacente ( actual , adyacente )
                if( !adyacente.darVisitado() ){        //si el vertice adyacente no fue visitado
                    relajacion( actual , adyacente , peso ); //realizamos el paso de relajacion
                }
            }
            }
        }

        System.out.println( "Distancias mas cortas iniciando en vertice " + inicial.darId() );
        System.out.println( "Distancia aproximada: " + suma );
        System.out.println( "Numero de vertices del camino: " + previo.size() );
        
        }catch(Exception e){
        	System.out.println(e.getMessage());
        }
    }
    
   
    

    
    //Impresion del camino mas corto desde el vertice inicial y final ingresados
   public void print( Vertice<String> destino ){
        if( previo.get(destino.darId()) != null )    //si aun poseo un vertice previo
            print( previo.get(destino.darId() ));  //recursivamente sigo explorando
        System.out.println( destino.darId() );        //terminada la recursion imprimo los vertices recorridos
    }
   public LinearProbingHash<String,Vertice<String>> darPrevio(){
	   return previo;
   }
    public int getNumberOfVertices() {
        return V;
    }

    public void setNumberOfVertices(int numeroDeVertices) {
        V = numeroDeVertices;
    }
}
