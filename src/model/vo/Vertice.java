package model.vo;

import model.data_structures.ArregloFlexible;

public class Vertice<K>  {
	private K identificacion;
	private InfoVertice infoVertice;
	private ArregloFlexible<Arco> arcos=new ArregloFlexible<Arco>(1);
	private boolean visitado=false;
	public Vertice(InfoVertice info, K id) {
		infoVertice=info;
		identificacion=id;
		
	}
	public K darId() {
		return identificacion;
	}
	public InfoVertice darInfovertice() {
		return infoVertice;
	}
	public ArregloFlexible<Arco> darArcos(){
		return arcos;
	}
	public void visitar(){
		visitado=true;
	}
	public boolean darVisitado(){
		return visitado;
	}
	public void anadirArco(Arco a) {
		arcos.agregarElem(a);
	}
	
	

}
