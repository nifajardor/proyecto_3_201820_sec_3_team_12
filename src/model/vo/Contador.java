package model.vo;

public class Contador {
	private int cantidad;
	public Contador() {
		cantidad=0;
	}
	public void incrementar() {
		cantidad++;
	}
	public void reset() {
		cantidad=0;
	}
}
