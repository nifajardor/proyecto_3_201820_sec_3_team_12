package model.vo;

import model.data_structures.ArregloFlexible;

public class InfoVertice {
	protected double lat;
	protected double lon;
	protected ArregloFlexible<Vertice<String>> adjacencias;
	
	public void adicionarCalle(Vertice<String> adicionar) {
		adjacencias.agregarElem(adicionar);
	}
	public ArregloFlexible<Vertice<String>> darAdjacencias(){
		return adjacencias;
	}
	public double darLat(){
		return lat;
	}
	public double darLon(){
		return lon;
	}
}
