package model.vo;

public class Promediar {
	private int cantidadElementos;
	private int acumuladoElementos;
	public Promediar() {
		cantidadElementos=0;
		acumuladoElementos=0;
	}
	public void incrementarCantidad(int cantidad) {
		cantidadElementos++;
		acumuladoElementos+=cantidad;
	}
	public double darPromedio() {
		if(cantidadElementos==0)cantidadElementos=1;
		return (acumuladoElementos/cantidadElementos);
	}
}
