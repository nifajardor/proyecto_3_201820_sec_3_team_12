package model.vo;

import model.data_structures.ArregloFlexible;

public class Interseccion extends InfoVertice{
	private int id;
	
	public Interseccion(int pId, double pLat, double pLong){
		id=pId;
		lat=pLat;
		lon=pLong;
		adjacencias=new ArregloFlexible<Vertice<String>>(401);
	}
	public int darId(){
		return id;
	}
	
	
}
