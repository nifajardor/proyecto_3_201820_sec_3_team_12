package model.logic;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import model.data_structures.*;
import sun.java2d.loops.DrawPath;

import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.swing.MapView;

import API.IDivvyTripsManager;


public class Mapa extends MapView {
	public static final String A1 = "A1";
	public static final String A2 = "A2";
	public static final String B1 = "B1";
	public static final String B2 = "B2";
	public static final String C1 = "C1";
	public static final String C2 = "C2";
	public static final String C3 = "C3";

	private static IDivvyTripsManager manager =new DivvyTripsManager();
	private Map map;
	public Mapa(String namme,String metodo)
	{


		JFrame frame = new JFrame(namme);
		setOnMapReadyHandler(new MapReadyHandler()
		{
			@Override
			public void onMapReady(MapStatus status) {
				if(status== MapStatus.MAP_STATUS_OK)
				{
					final Map mapa = getMap();
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					mapOptions.setMapTypeControlOptions(controlOptions);
					mapa.setOptions(mapOptions);
					mapa.setCenter(new LatLng(41.8781, -87.6298));
					mapa.setZoom(11.0); 
					Marker marker = new Marker(map);
					marker.setVisible(true);
					marker.setPosition(mapa.getCenter());
					marker.setVisible(true);
					switch(metodo) {
					case A1:
						hacerA1();
						break;
					case A2:
						hacerA2();
						break;
					case B1:
						hacerB1();
						break;
					case B2:
						hacerB2();
						break;
					case C1:
						hacerC1();
						break;
					case C2:
						hacerC2();
						break;
					case C3:
						hacerC3();
						break;
					}
				}
			}
		});
		frame.add(this,BorderLayout.CENTER);
		frame.setSize(900, 600);
		frame.setVisible(true);

	}
	
	public void drawCircle(Map mappp, LatLng coords,int radio) {
		CircleOptions ops = new CircleOptions();
		ops.setFillOpacity(0);
		ops.setStrokeColor(Color.BLACK.toString());
		ops.setStrokeWeight(1.0);
		Circle circulo = new Circle(mappp);
		circulo.setCenter(coords);
		circulo.setRadius(radio);
		
	}
	public void drawLine(Map mappp,LatLng[] camino) {
		Polyline linea = new Polyline(mappp);
		linea.setPath(camino);
		
		PolylineOptions ops = new PolylineOptions();
		ops.setGeodesic(true);
		ops.setStrokeColor(Color.BLACK.toString());
		ops.setStrokeOpacity(2.0);
		linea.setOptions(ops);
	}
	
	public void drawMarker( Map map, LatLng coords, int diameter) {
		Marker marker = new Marker(map);
		marker.setPosition(coords);

		Circle circle = new Circle(map);
		circle.setCenter(coords);
		circle.setRadius(diameter);
	}
	public void hacerA1() {
		Queue<String> coords = manager.darQueueA1();
		while(!coords.isEmpty()) {
			String s = coords.dequeue();
			String[] latLon = s.split(",");
			double latitud = Double.parseDouble(latLon[0]);
			double longitud = Double.parseDouble(latLon[1]);

			LatLng c = new LatLng(latitud,longitud);
			LatLng c2 = new LatLng(latitud,longitud);

			drawMarker(map, c, 20);
			drawMarker(map, c2, 20);
		}
	}
	public void hacerA2() {
		Queue<String> coords = manager.darQueueA2();
		while(!coords.isEmpty()) {
			String s = coords.dequeue();
			String[] latLon = s.split(",");
			double latitud = Double.parseDouble(latLon[0]);
			double longitud = Double.parseDouble(latLon[1]);

			LatLng c = new LatLng(latitud,longitud);
			LatLng c2 = new LatLng(latitud,longitud);

			drawMarker(map, c, 20);
			drawMarker(map, c2, 20);
		}
	}
	public void hacerB1() {

		Queue<String> coords = manager.darQueueB1();
		while(!coords.isEmpty()) {
			String s = coords.dequeue();
			String[] latLon = s.split(",");
			double latitud = Double.parseDouble(latLon[0]);
			double longitud = Double.parseDouble(latLon[1]);

			LatLng c = new LatLng(latitud,longitud);
			LatLng c2 = new LatLng(latitud,longitud);

			drawMarker(map, c, 20);
			drawMarker(map, c2, 20);
			
		}


	}
	public void hacerB2() {
		Queue<String>[] caminos = manager.retCaminosB2();
		for(int i=0;i<caminos.length;i++) {
			Queue<String> actual = caminos[i];
			while(!actual.isEmpty()) {
				String act = actual.dequeue();
				String[] c =act.split(",");
				double la1 = Double.parseDouble(c[0]);
				double lo1 = Double.parseDouble(c[1]);
				double la2 = Double.parseDouble(c[2]);
				double lo2 = Double.parseDouble(c[3]);
				LatLng co1 = new LatLng(la1,lo1);
				LatLng co2 = new LatLng(la2,lo2);
				LatLng[] cam = new LatLng[2];
				cam[0] = co1;
				cam[1] = co2;
				drawCircle(map, co1, 10);
				drawCircle(map,co2,10);
				drawLine(map, cam);
			}
		}
	}	 
	public void hacerC1() {
		Queue<String> estaciones = manager.darCoordenadasEstaciones();
		Queue<String> arcos = manager.darCoordenadasArcoDirigido();
		while(!estaciones.isEmpty()) {
			String coords = estaciones.dequeue();
			String[] c = coords.split(",");
			double la = Double.parseDouble(c[0]);
			double lo = Double.parseDouble(c[1]);
			drawCircle(map, new LatLng(la,lo), 10);
		}
		while(!arcos.isEmpty()) {
			String[] coooooo = arcos.dequeue().split(",");
			double la1 = Double.parseDouble(coooooo[0]);
			double lo1 = Double.parseDouble(coooooo[1]);
			double la2 = Double.parseDouble(coooooo[2]);
			double lo2 = Double.parseDouble(coooooo[3]);
			LatLng[] coords = new LatLng[2];
			coords[0] = new LatLng(la1,lo1);
			coords[1] = new LatLng(la2,lo2);
			drawLine(map, coords);
		}
	}
	public void hacerC2() {

	}
	public void hacerC3() {

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Mapa temp = new Mapa("Prueba",B1);
	}

}