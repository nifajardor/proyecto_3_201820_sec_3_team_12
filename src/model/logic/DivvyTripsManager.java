package model.logic;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;

import com.opencsv.CSVReader;

import API.IDivvyTripsManager;
import model.data_structures.*;
import model.vo.*;

public class DivvyTripsManager implements IDivvyTripsManager {
	public static final String RUTA_STATIONS1="data/Divvy_Stations_2017_Q1Q2.csv";
	public static final String RUTA_STATIONS2="data/Divvy_Stations_2017_Q3Q4.csv";
	public static final String RUTA_TRIPS1="data/Divvy_Trips_2017_Q1.csv";
	public static final String RUTA_TRIPS2="data/Divvy_Trips_2017_Q2.csv";
	public static final String RUTA_TRIPS3="data/Divvy_Trips_2017_Q3.csv";
	public static final String RUTA_TRIPS4="data/Divvy_Trips_2017_Q4.csv";
	public static final int RADIO_TIERRA = 6371;
	private LinearProbingHash<String,Vertice<String>> hashNodos;
	private ArregloFlexible<Vertice<String>> vertices;
	private ArregloFlexible<Arco> arregloCalles;
	private ArregloFlexible<Interseccion> arregloNodos;
	private ArregloFlexible<Station> arregloStats;
	private Queue<Trip> tripQueue;
	public Queue<Station> queueB2;
	private LinearProbingHash<String,Station> hashStats;
	private Digraph grafoDirigido;
	private Mapa mapa;
	public Queue<String> queueB1;
	public Queue<String> queueA1=new Queue<String>();
	public Queue<String> queueA2=new Queue<String>();
	private int ccc;
	private ArregloFlexible<String> arcosC1;
	private LinearProbingHash<String,Promediar> pesosC1;
	 private LinearProbingHash<String,Vertice<String>> previo  =new  LinearProbingHash<String,Vertice<String>>();
	private Queue<Vertice<String>> cola=new Queue<Vertice<String>>();
	private LinearProbingHash<String,Boolean> visitados=new LinearProbingHash<String,Boolean>();
	private ArregloFlexible<String> clavesVertices;
	private LinearProbingHash<String, String> hashClaves;
	private Grafo<String,Vertice<String>,Arco> grafo=new Grafo<String,Vertice<String>,Arco>();
	public void cargarSistema() {
		// TODO Auto-generated method stub
		arregloStats = new ArregloFlexible<Station>(700);
		tripQueue = new Queue<Trip>();
		hashStats = new LinearProbingHash<String,Station>(800);
		int totalViajes = 0;
		int totalStats = 0;
		int[] infoGrafo=importarJson();
		arregloStats = new ArregloFlexible<Station>(700);
		totalViajes+=cargarViajes(RUTA_TRIPS1);
		totalViajes+=cargarViajes(RUTA_TRIPS2);
		totalViajes+=cargarViajes(RUTA_TRIPS3);
		totalViajes+=cargarViajes(RUTA_TRIPS4);
		totalStats+=cargarStats(RUTA_STATIONS1);
		totalStats+=cargarStats(RUTA_STATIONS2);
		
		System.out.println("Se han cargado "+infoGrafo[0]+" v�rtices y "+infoGrafo[1]+" arcos");
		System.out.println("Se han cargado "+totalViajes+" viajes");
		System.out.println("Se han cargado "+totalStats+" estaciones");
	}
	public int cargarViajes(String rutaTrips) {
		int ret =0;
		try {
			CSVReader lectorTrips = new CSVReader(new FileReader(rutaTrips));
			String[] trip;
			trip = lectorTrips.readNext();
			while((trip = lectorTrips.readNext()) != null) {
				int id = Integer.parseInt(trip[0]);
				int startStatID = Integer.parseInt(trip[5]);
				int endStatID = Integer.parseInt(trip[7]);
				Trip actual = new Trip(id,startStatID, endStatID);
				tripQueue.enqueue(actual);
				
				ret++;
			}
			lectorTrips.close();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ret;
	}
	public int cargarStats(String rutaStats) {
		int ret =0;
		try {
			CSVReader lectorStats = new CSVReader(new FileReader(rutaStats));
			String[] stat;
			stat = lectorStats.readNext();
			while((stat = lectorStats.readNext())!= null) {
				int id=Integer.parseInt(stat[0]);
				String nombre = stat[1];
				double lat = Double.parseDouble(stat[3]);
				double lon = Double.parseDouble(stat[4]);
				
				if(hashStats.get(id+"")==null) {
					ret++;
					Station actual = new Station(id, lat, lon, nombre);
					hashStats.put(id+"", actual);
					arregloStats.agregarElem(actual);
					System.out.println(actual.toString());
				}
				
			}
			
			lectorStats.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	public int[] importarJson() {
		int[] ret = new int[2];
		ret[0]=0;
		ret[1]=0;
		// TODO Auto-generated method stub
		int totalVertices =0;
		int totalArcos = 0;
		hashNodos = new LinearProbingHash<String, Vertice<String>>(330000);
		vertices = new ArregloFlexible<Vertice<String>>(330000);
		arregloCalles = new  ArregloFlexible<Arco>(330000);
		arregloNodos = new ArregloFlexible<Interseccion>(330000);
		CSVReader lector;
		try {
			lector = new CSVReader(new FileReader("data/infoGrafo.csv"));
			String[] info = lector.readNext(); 
			boolean yaVertices=false;
			while(!yaVertices) {
				info = lector.readNext();
				if(info[0].equals("CAMBIO")) {
					yaVertices = true;
				}
				else {
					totalVertices++;
					String id = info[0];
					double latitud = Double.parseDouble(info[1]);
					double longitud = Double.parseDouble(info[2]);
					int idd = Integer.parseInt(id.split("-")[1]);
					if(id.contains("I")) {
						Interseccion inter=new Interseccion(idd, latitud ,longitud);
						arregloNodos.agregarElem(inter);
						hashNodos.put(id, new Vertice<String>(inter,id));
						vertices.agregarElem(new Vertice<String>(inter,id));
						grafo.addVertex(id,new Vertice<String>(inter,id));
						System.out.println("I-"+id+":"+latitud+"-"+longitud);
					}else {
						Station estacion = new Station(idd,latitud,longitud);
						System.out.println("Se carg� la estaci�n: "+id);
						hashNodos.put(id, new Vertice<String>(estacion,id));
						vertices.agregarElem(new Vertice<String>(estacion,id));
						grafo.addVertex(id, new Vertice<String>(estacion,id));
						//statQueue.enqueue(nodo);
						try {
							arregloStats.agregarElem(estacion);
						}catch(Exception e) {
							System.out.println("");
						}
					}
				}	
			}
			while((info = lector.readNext())!= null) {
				totalArcos++;
				String id = info[0];
				Vertice<String> inicial = hashNodos.get(info[0]);
				Vertice<String> otro = hashNodos.get(info[1]);
				double distancia = Double.parseDouble(info[2]);
				Arco anadir = new Arco(inicial, otro,distancia);
				Arco anadir2 = new Arco(otro, inicial,distancia);
				grafo.getInfoVertex(inicial.darId()).anadirArco(anadir);
				grafo.getInfoVertex(otro.darId()).anadirArco(anadir);
				grafo.getInfoVertex(otro.darId()).darInfovertice().adicionarCalle(inicial);
				grafo.getInfoVertex(inicial.darId()).darInfovertice().adicionarCalle(otro);
				
				arregloCalles.agregarElem(anadir);
				grafo.addEdge(inicial.darId(), otro.darId(),anadir);
				grafo.addEdge(otro.darId(), inicial.darId(),anadir2);
				inicial.darId();
				otro.darId();
				System.out.println("V1:"+inicial.darId()+"V2:"+otro.darId()+"D:"+distancia);
			}

			lector.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ret[0]=totalVertices;
		ret[1]=totalArcos;
		return ret;
	}
	public String darEstacionMasCercano(double lat, double lon) {
		String ret ="";
		double menorDistancia = Double.MAX_VALUE;
		for(int i=0;i<vertices.darTamAct();i++) {
			Vertice<String> actual = (Vertice<String>) vertices.darElementoI(i);
			if(actual.darId().contains("I")) {
				double lat2 = actual.darInfovertice().darLat();
				double lon2 = actual.darInfovertice().darLon();
				double distanciaActual= distancia(lat, lon, lat2, lon2);
				if(distanciaActual<=menorDistancia) {
					menorDistancia=distanciaActual;
					ret = actual.darId();
					
				}
			}
		}
		//System.out.println(ret);
		return ret;
	}
	public String darEstacionMasCercanoConsola(double lat, double lon) {
		String ret ="";
		double menorDistancia = Double.MAX_VALUE;
		for(int i=0;i<vertices.darTamAct();i++) {
			Vertice<String> actual = (Vertice<String>) vertices.darElementoI(i);
			if(actual.darId().contains("S")) {
				double lat2 = actual.darInfovertice().darLat();
				double lon2 = actual.darInfovertice().darLon();
				double distanciaActual= distancia(lat, lon, lat2, lon2);
				if(distanciaActual<=menorDistancia) {
					menorDistancia=distanciaActual;
					ret = actual.darId();
					
				}
			}
		}
		//System.out.println(ret);
		return ret;
	}
	public static double distancia(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return RADIO_TIERRA*1000 * c; // <-- d
	}
	public static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	public VOPath A1_menorDistancia(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		//TODO Visualizacion Mapa
		String id1=darEstacionMasCercano(latInicial, lonInicial);
		String id2=darEstacionMasCercano(latFinal, lonFinal);
		Dijkstra dijkstra=new Dijkstra(grafo);
		try{
		dijkstra.dijkstra(grafo.getInfoVertex(id1));
		dijkstra.print(grafo.getInfoVertex(id2));
		
		System.out.println("La estacion m�s cercana al origen es: " +darEstacionMasCercanoConsola(latInicial, lonInicial));
		System.out.println("La estacion m�s cercana al destino es: " +darEstacionMasCercanoConsola(latFinal, lonFinal));
		for(String s:dijkstra.darPrevio().keys()){
		
			queueA1.enqueue(grafo.getInfoVertex(s).darInfovertice().darLat()+","+grafo.getInfoVertex(s).darInfovertice().darLon());
		}
		mapa=new Mapa("Camino de menor distancia", Mapa.A1);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return null;
	}
	public Queue<String> darQueueA1(){
		return queueA1;
	}
	public Queue<String> darQueueA2(){
		return queueA2;
	}
	public VOPath A2_menorNumVertices(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		
		//TODO Visualizacion Mapa
		try{
		String id1=darEstacionMasCercano(latInicial, lonInicial);
		String id2=darEstacionMasCercano(latFinal, lonFinal);
		BFS(grafo.getInfoVertex(id1));
		print(grafo.getInfoVertex(id2));
		System.out.println("El numero de vertices del camino es: " +previo.size());
		System.out.println("La estacion m�s cercana al origen es: " +darEstacionMasCercanoConsola(latInicial, lonInicial));
		System.out.println("La estacion m�s cercana al destino es: " +darEstacionMasCercanoConsola(latFinal, lonFinal));
		for(String s:previo.keys()){
			
			queueA2.enqueue(grafo.getInfoVertex(s).darInfovertice().darLat()+","+grafo.getInfoVertex(s).darInfovertice().darLon());
		}
		mapa=new Mapa("Camino con menor numero de vertices", Mapa.A2);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return null;
	}

	public void BFS(Vertice<String> inicial){
		try {
			visitados.put(inicial.darId(), true);
			cola.enqueue(inicial);
			while(!cola.isEmpty()){
				inicial=cola.dequeue();
				System.out.println(inicial.darId());
				for(int i=0;i<inicial.darInfovertice().darAdjacencias().darTamAct();i++){
					Vertice<String> adyacente=inicial.darInfovertice().darAdjacencias().darElementoI(i);
					if(visitados.get(adyacente.darId())==null){
						visitados.put(adyacente.darId(),true);
						previo.put(adyacente.darId(), inicial);
						cola.enqueue(adyacente);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 public void print( Vertice<String> destino ){
	        if( previo.get(destino.darId()) != null )    //si aun poseo un vertice previo
	            print( previo.get(destino.darId() ));  //recursivamente sigo explorando
	        System.out.println( destino.darId() );        //terminada la recursion imprimo los vertices recorridos
	    }
	
	public Queue<Station> B1_estacionesCongestionadas(int n) {
		ccc=0;
		//TODO Visualizacion Mapa
		Queue<Trip> temp = new Queue<Trip>();
		int[] salida = new int[800];
		int[] llegada = new int[800];
		for(int i=0;i<salida.length;i++) {
			salida[i]=0;
			llegada[i]=0;
		}
		while(!tripQueue.isEmpty()) {
			Trip act = tripQueue.dequeue();
			temp.enqueue(act);
			int start = act.getStartStationId();
			int end = act.getEndStationId();
			salida[start]++;
			llegada[end]++;
		}
		tripQueue = temp;
		Queue<Station> estacionesCongestionadas = new Queue<Station>();
		Queue<String> coordenadas = new Queue<String>();
		for(int i=0;i<n;i++) {
			int mayorCongestion =0;
			int indiceCongestion=0;
			for(int j=0;j<salida.length;j++) {
				int numero = salida[j]+llegada[j];
				if(numero>=mayorCongestion) {
					mayorCongestion=numero;
					indiceCongestion=j;
				}
			}
			int salidas = salida[indiceCongestion];
			int llegadas = llegada[indiceCongestion];
			salida[indiceCongestion]=0;
			llegada[indiceCongestion]=0;
			Station actual = hashStats.get(""+indiceCongestion);
			actual.asignarSalidaLlegada(salidas, llegadas);
			hashStats.delete(""+indiceCongestion);
			try {
				hashStats.put(""+indiceCongestion, actual);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			estacionesCongestionadas.enqueue(actual);
			double lat = actual.darLat();
			double lon = actual.darLon();
			String coord = lat+","+lon;
			coordenadas.enqueue(coord);
			ccc++;
		}
		queueB1 = coordenadas;
		mapa = new Mapa("Estaciones mas congestionadas", Mapa.B1);
		queueB2 = estacionesCongestionadas;
		return estacionesCongestionadas;
	}
	public Queue<String> darQueueB1(){
		return queueB1;
	}
	public IList<VOPath> B2_rutasMinimas(Queue<Station> stations) {
		//TODO Visualizacion Mapa
		Station[] estacionesCongestionadas = new Station[ccc];
		ccc = darCantidadEspacios(ccc);
		caminosB2 = new Queue[ccc];
		for(int i =0;i<caminosB2.length;i++) {
			caminosB2[i]=new Queue<String>();
		}
		Dijkstra d = new Dijkstra(grafo);
		Queue<Station> q = new Queue<Station>();
		
		while(!queueB2.isEmpty()) {
			Station origen = queueB2.dequeue();
			ccc--;
			q = queueB2;
			double lao = origen.darLat();
			double loo = origen.darLon();
			String idO = darEstacionMasCercano(lao, loo);
			try {
				d.dijkstra(grafo.getInfoVertex(idO));
				LinearProbingHash<String,Vertice<String>> camino = d.darPrevio();
				while(!q.isEmpty()) {
					Station destino = q.dequeue();
					double lad = origen.darLat();
					double lod = origen.darLon();
					String idD = darEstacionMasCercano(lad, lod);
					darCaminoB2(grafo.getInfoVertex(idD),caminosB2[ccc],camino);
					d.print(grafo.getInfoVertex(idD));
				}
			}catch(Exception e) {	
			}	
		}
		mapa = new Mapa("Dar recorrido mas corto entre las estaciones mas congestionadas", Mapa.B2);
		return null;
	}
	public Queue<String>[] retCaminosB2(){
		return caminosB2;
	}
	public void darCaminoB2(Vertice<String> destino,Queue<String> camino,LinearProbingHash<String,Vertice<String>> previo) {
		if(previo.get(destino.darId())!= null) {
			Vertice<String> origen = previo.get(destino.darId());
			String llave = origen.darInfovertice().darLat()+","+origen.darInfovertice().darLon()+","+destino.darInfovertice().darLat()+","+destino.darInfovertice().darLon();
			camino.enqueue(llave);
			darCaminoB2(previo.get(destino.darId()), camino, previo);
		}
	}
	public int darCantidadEspacios(int numero) {
		int ret =0;
		for(int i=0;i<numero;i++) {
			ret+=i;
		}
		return ret;
	}
	private Queue<String>[] caminosB2;

	public void C1_grafoEstaciones() {
		//TODO Visualizacion Mapa
		grafoDirigido = new Digraph(585);
		clavesVertices = new ArregloFlexible<String>(585);
		hashClaves = new LinearProbingHash<String,String>(585);
		for(int i=0;i<585;i++) {
			Station act = (Station) arregloStats.darElementoI(i);
			String llavvee = act.getStationId()+"";
			clavesVertices.agregarElem(llavvee);
			int val = i;
			try {
				hashClaves.put(llavvee,val+"" );
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LinearProbingHash<String,Promediar> promedios = new LinearProbingHash<String,Promediar>(800);
		ArregloFlexible<String> arcos = new ArregloFlexible<String>(800);
		Queue<Trip> temp = tripQueue;
		while(!temp.isEmpty()) {
			Trip act = temp.dequeue();
			int salida = act.getStartStationId();
			int llegada = act.getEndStationId();
			int duracion = act.getTripDuration();
			String llave = salida+"-"+llegada;
			if(promedios.get(llave)==null) {
				try {
					promedios.put(llave, new Promediar());
					promedios.get(llave).incrementarCantidad(duracion);
					arcos.agregarElem(llave);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				promedios.get(llave).incrementarCantidad(duracion);
			}
		}
		//En este punto el ArregloFlexible arcos tiene en cada posicion un arco del grafo dirijido
		//donde el primer valor es la cola del arco y el segundo la cabeza. 
		//El hashTable promedios tiene como llaves cada arco del arreglo anterior, y como
		//valores tiene un objeto de la clase promediar, de donde se puede sacar el promedio
		//de los tiempos de viaje
		for(int i=0;i<arcos.darTamAct();i++) {
			String llave = (String) arcos.darElementoI(i);
			String[] datos = llave.split("-");
			int salida = Integer.parseInt(hashClaves.get(datos[0]));
			int llegada = Integer.parseInt(hashClaves.get(datos[1]));
			double peso = promedios.get(llave).darPromedio();
			grafoDirigido.addEdge(salida, llegada, peso+"");
		}
		System.out.println(grafoDirigido.toString());
		System.out.println("El grafo tiene "+grafoDirigido.V()+" v�rtices");
		System.out.println("El grafo tiene "+grafoDirigido.E()+" arcos");
	}
	
	public void C1_persistirGrafoEstaciones(){
		//TODO Visualizacion Mapa
		try {
			PrintWriter writer = new PrintWriter(new File("data/infoGrafoDir.json"));
			writer.println(" ");
			for(int i=0;i<clavesVertices.darTamAct();i++) {
				String llave = (String) clavesVertices.darElementoI(i);
				writer.println(i+"-"+llave);
			}
			writer.println("CAMBIO-CAMBIO");
			for(int i=0;i<clavesVertices.darTamAct();i++) {
				String llave = (String) clavesVertices.darElementoI(i);
				Station act = hashStats.get(llave);
				int id = act.getStationId();
				String nom = act.getStationName();
				double lat = act.darLat();
				double lon = act.darLon();
				writer.println(nom+"-"+id+"-"+lat+"-"+lon);
			}
			writer.println("CAMBIO-CAMBIO");
			ArregloFlexible<String> listaARcos = grafoDirigido.darListaArcos();
			LinearProbingHash<String,String> pesoArcos = grafoDirigido.darPesosArcos();
			for(int i=0;i<listaARcos.darTamAct();i++) {
				String llave = (String) listaARcos.darElementoI(i);
				String peso = pesoArcos.get(llave);
				writer.println(llave+"-"+peso);
			}
			writer.close();
			System.out.println("_____________________");
			System.out.println("Se han guardado los datos en el archivo infoGrafoDir.json");
			System.out.println("El siguiente es el formato");
			System.out.println("Codificacion de vertices. Donde esta del modo X-Y");
			System.out.println("Donde X es el numero del vertice y Y es el numero de la estacion correspondiente");
			System.out.println("CAMBIO-CAMBIO");
			System.out.println("Vertices Estacion en el formato A-B-C-D");
			System.out.println("Donde A es el nombre de la estacion");
			System.out.println("B es el id de la estacion");
			System.out.println("C es la latidud de la posicion");
			System.out.println("D es la longitud de la posicion");
			System.out.println("CAMBIO-CAMBIO");
			System.out.println("Arcos y sus pesos en el formato X-Y-Z");
			System.out.println("Donde X es la estacion de salida, Y es la estacion de llegada");
			System.out.println("y Z es el peso del arco (la duracion promedio)");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public Queue<String> darCoordenadasArcoDirigido(){
		Queue<String> ret = new Queue<String>();
		for(int i=0;i<arcosC1.darTamAct();i++) {
			String cosas = (String) arcosC1.darElementoI(i);
			String[] path = cosas.split("-");
			Station salida = hashStats.get(path[0]);
			Station llegada = hashStats.get(path[1]);
			ret.enqueue(darCoordenadasEstacion(salida)+","+darCoordenadasEstacion(llegada));
		}
		return ret;
	}
	public Queue<String> darCoordenadasEstaciones(){
		Queue<String> ret = new Queue<String>();
		for(int i=0;i<arregloStats.darTamAct();i++) {
			Station act = (Station) arregloStats.darElementoI(i);
			String coords = darCoordenadasEstacion(act);
			ret.enqueue(coords);
		}
		return ret;
	}
	public String darCoordenadasEstacion(Station s) {
		double lat = s.darLat();
		double lon = s.darLon();
		return lat+","+lon;
	}
	public IList<VOComponenteFuertementeConectada> C2_componentesFuertementeConectados(IGraph grafo) {
		//TODO Visualizacion Mapa
		grafoDirigido.darComponentesFuertementeConectadas();
		return null;
	}

	public void C3_pintarGrafoEstaciones(IGraph grafoEstaciones) {
		//TODO Visualizacion Mapa!
		mapa=new Mapa("Grafo dirigido", mapa.A2);
	}
}
